#ifndef _CONFIG_H_
#define _CONFIG_H_

#define TRUE 1
#define FALSE 0
const char *FONT           = "Monospace 9";
int SCROLL_BACK_LINES      = 2048;
int SCROLL_ON_OUTPUT       = FALSE;
int SCROLL_ON_KEYSTROKE    = TRUE;
int HIDE_MOUSE_WHEN_TYPING = FALSE;
int AUDIBLE_BELL           = FALSE;

#endif
