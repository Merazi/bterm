#include "config.h"
#include <vte/vte.h>

static void child_ready(VteTerminal *terminal, GPid pid, GError *error, gpointer user_data)
{
    if (!terminal) return;
    if (pid == -1) gtk_main_quit();
}

static gboolean on_title_changed(GtkWidget *terminal, gpointer user_data)
{
    GtkWindow *window = user_data;
    gtk_window_set_title(window,
            vte_terminal_get_window_title(VTE_TERMINAL(terminal))?:"bterm");
    return TRUE;
}

int main(int argc, char *argv[])
{
    GtkWidget *window, *terminal;

    /* Initialise GTK, the window and the terminal */
    gtk_init(&argc, &argv);
    terminal = vte_terminal_new();
    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "bterm");

    /* fonts */
    PangoFontDescription * desc = pango_font_description_from_string(FONT);
    vte_terminal_set_font(VTE_TERMINAL(terminal), desc);

    /* behaviour */
    vte_terminal_set_scrollback_lines(VTE_TERMINAL(terminal), SCROLL_BACK_LINES);
    vte_terminal_set_scroll_on_output(VTE_TERMINAL(terminal), SCROLL_ON_OUTPUT);
    vte_terminal_set_scroll_on_keystroke(VTE_TERMINAL(terminal), SCROLL_ON_KEYSTROKE);
    vte_terminal_set_mouse_autohide(VTE_TERMINAL(terminal), HIDE_MOUSE_WHEN_TYPING);
    vte_terminal_set_audible_bell(VTE_TERMINAL(terminal), AUDIBLE_BELL);

    /* colors TODO*/
#define CLR_R(x)   (((x) & 0xff0000) >> 16)
#define CLR_G(x)   (((x) & 0x00ff00) >>  8)
#define CLR_B(x)   (((x) & 0x0000ff) >>  0)
#define CLR_16(x)  ((double)(x) / 0xff)
#define CLR_GDK(x) (const GdkRGBA){ .red = CLR_16(CLR_R(x)), .green = CLR_16(CLR_G(x)), .blue = CLR_16(CLR_B(x)), .alpha = 0.2 }
    vte_terminal_set_colors(VTE_TERMINAL(terminal),
            &CLR_GDK(0xF8F8F2),
            &(GdkRGBA){ .alpha = 0.85 },
            (const GdkRGBA[]){
            CLR_GDK(0x000000),
            CLR_GDK(0xFF5555),
            CLR_GDK(0x50FA7B),
            CLR_GDK(0xF1FA8C),
            CLR_GDK(0xBD93F9),
            CLR_GDK(0xFF79C6),
            CLR_GDK(0x8BE9FD),
            CLR_GDK(0xBFBFBF),
            CLR_GDK(0x4D4D4D),
            CLR_GDK(0xFF6E67),
            CLR_GDK(0x5AF78E),
            CLR_GDK(0xF4F99D),
            CLR_GDK(0xCAA9FA),
            CLR_GDK(0xFF92D0),
            CLR_GDK(0x9AEDFE),
            CLR_GDK(0xE6E6E6)
            }, 16);

    /* Start a new shell */
    gchar **envp = g_get_environ();
    gchar **command = (gchar *[]){g_strdup(g_environ_getenv(envp, "SHELL")), NULL };
    g_strfreev(envp);
    vte_terminal_spawn_async(VTE_TERMINAL(terminal),
            VTE_PTY_DEFAULT,
            NULL,         /* working directory  */
            command,      /* command */
            NULL,         /* environment */
            0,            /* spawn flags */
            NULL, NULL,   /* child setup */
            NULL,         /* child pid */
            -1,           /* timeout */
            NULL,         /* cancellable */
            child_ready,  /* callback */
            NULL);        /* user_data */

    /* Connect some signals */
    g_signal_connect(terminal, "window-title-changed", 
            G_CALLBACK(on_title_changed), GTK_WINDOW(window));
    g_signal_connect(window, "delete-event", gtk_main_quit, NULL);
    g_signal_connect(terminal, "child-exited", gtk_main_quit, NULL);

    /* Put widgets together and run the main loop */
    gtk_container_add(GTK_CONTAINER(window), terminal);
    gtk_widget_show_all(window);
    gtk_main();
}
